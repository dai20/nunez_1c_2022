/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA INÉS NUÑEZ
*/

/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 *  El programa recibe un dato en código decimal, separa en dígitos a dicho dato,
 *  convierte cada dígito a código BCD. Guarda la información en bits en un vector y
 *  además mapea los puertos, para que cada dígito sea mostrado en un display LCD.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/03/2022 | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |         						 |
 *
 * @author DAIANA INÉS NUÑEZ
 *
 */

#ifndef _EJERC6_H
#define _EJERC6_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include "gpio.h"
#include <stdint.h>

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[macros and definitions]=================================*/

/*==================[typedef]================================================*/
typedef struct
{
	gpio_t pin;				/*!< GPIO pin number */
	io_t dir;				/*!< GPIO direction '0' IN;  '1' OUT */
} gpioConf_t;

/*==================[internal functions declaration]=========================*/
/**
@fn void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number)
@brief Separa en cantidad de dígitos un nro. Decimal y
Convierte cada dígito Decimal a BCD.
@param[in] el nro. decimal a convertir.
@param[in] la cantidad de dígitos de salida.
@param[in] un puntero a un arreglo donde se almacenan los n dígitos.
 **/
void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number);

/**
@fn void BcdGpio(uint8_t bcd, gpioConf_t *portPtr)
@brief Asigna los bits de un dígito en código BCD a un vector. Además indexa dicho vector,
para operar sobre el puerto y pin que corresponda.
@param[in] un dígito en código BCD.
@param[in] un puntero a una estructura del tipo gpioConf_t.
 **/
void BcdGpio(uint8_t bcd, gpioConf_t *portPtr);

/**
@fn ShowDisPlay (uint32_t dato, uint8_t numberOfDigits, gpioConf_t *portPtr , gpioConf_t *portPtr2)
@brief Recibe un dato en decimal, lo divide en dígitos, los convierte a código BDC, y los muestra en
un display LCD.
@param[in] el nro. decimal a convertir.
@param[in] la cantidad de dígitos de salida.
@param[in] un puntero a una estructura del tipo gpioConf_t.
@param[in] otro puntero a una estructura del tipo gpioConf_t, dicho vector mapea
los puertos con el dígito del LCD donde se mostrará cada dato.
 **/
void ShowDisPlay (uint32_t dato, uint8_t numberOfDigits, gpioConf_t *portPtr , gpioConf_t *portPtr2);


/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EJERC6_H */
