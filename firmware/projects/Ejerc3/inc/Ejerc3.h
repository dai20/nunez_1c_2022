/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA INÉS NUÑEZ
*/

/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * El programa en función del modo que recibe: enciende (ON) , apaga (OFF) o cambia (TOGGLE) el estado de un led determinado.
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 10/03/2022 | Document creation		                         |
 * |			| 							                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA INÉS NUÑEZ.
 *
 */

#ifndef _EJERC3_H
#define _EJERC3_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[macros and definitions]=================================*/

#define COUNT_DELAY 3000000

/*==================[typedef]================================================*/
typedef struct leds
{
	uint8_t n_led;       // indica el número de led a controlar.
	uint8_t n_ciclos;   //  indica la cantidad de ciclos de encendido/apagado.
	uint32_t periodo;    //  indica el tiempo de cada ciclo.
	uint8_t mode;       //  ON 1, OFF 0, TOGGLE 2.
} my_leds;

/*==================[internal functions declaration]=========================*/

/**
@fn void myDelay(uint32_t periodo)
@brief Genera un retardo deacuerdo al valor ingresado del periodo.
@param[in] valor del periodo deseado.
**/
void myDelay(uint32_t periodo);

/**
@fn void myToggle (uint8_t led, uint8_t ciclos, uint32_t periodo)
@brief Realiza el cambio del estado del led y genera un retardo
deacuerdo al valor ingresado del periodo.
@param[in] nro. del led a modificar.
@param[in]  nro. de ciclos deseados.
@param[in] valor del periodo deseado.
**/
void myToggle (uint8_t led, uint8_t ciclos, uint32_t periodo);

/**
@fn void update( my_leds *ledPtr)
@brief Realiza la actualización del modo del Led.
@param[in] un puntero a la estructura del tipo my_leds.
 **/
void update( my_leds *ledPtr);


/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EJERC3_H */
