/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*
 * *! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 17/03/2022 | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA INÉS NUÑEZ
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Ejerc3.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include <stdio.h>
#include <stdint.h> //se utiliza para definir los tamaños de tipos enteros.

/*#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>
*/

/*==================[macros and definitions]=================================*/
typedef struct leds
{
	uint8_t n_led;       // indica el número de led a controlar.
	uint8_t n_ciclos;   //  indica la cantidad de ciclos de encendido/apagado.
	uint32_t periodo;    //  indica el tiempo de cada ciclo.
	uint8_t mode;       //  ON 1, OFF 0, TOGGLE 2.
} my_leds;

#define COUNT_DELAY 3000000
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void myDelay(uint32_t periodo)
{
	uint32_t i;

	for(i=0; i<=periodo; i++)
	{
		   asm  ("nop");
	}
}

void myToggle (uint8_t led, uint8_t ciclos, uint32_t periodo)
{
	uint8_t i=0;

	while(i<=ciclos)
	{
		LedToggle(led);
		myDelay(periodo);
		i++;
	}
}



void update( my_leds *ledPtr)//actualización del modo del Led.
{
	switch(ledPtr->mode)
	{
		case(0):
			LedOff(ledPtr->n_led);
			myDelay(ledPtr->periodo);
		break;
		case(1):
			LedOn(ledPtr->n_led);
			myDelay(ledPtr->periodo);
		break;
		case(2):
			myToggle (ledPtr->n_led,ledPtr->n_ciclos,ledPtr->periodo);
			myDelay(ledPtr->periodo);
			break;
	}
}



/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	my_leds led, *ledPtr;

	led.n_led=1;
	led.n_ciclos=10;
	led.periodo=2000000;
	led.mode=2;

	ledPtr = &led;

	SystemClockInit();
	LedsInit();

	update(ledPtr);

	myDelay(COUNT_DELAY);
	
	return 0;
}

/*==================[end of file]============================================*/

