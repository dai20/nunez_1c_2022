/* Cátedra: Electrónica Programable
* FIUNER - 2018
* Autor/es:
* JMReta - jmreta@ingenieria.uner.edu.ar
*
*
*
* Revisión:
* 07-02-18: Versión inicial
* 01-04-19: V1.1 SM
*
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*
 * *! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 17/03/2022 | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA INÉS NUÑEZ
 */
/*==================[inclusions]=============================================*/
#include "../inc/Ejerc4.h"       /* <= own header */
#include "systemclock.h"
#include <stdio.h>
#include <stdint.h> //se utiliza para definir los tamaños de tipos enteros.

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number)// Decimal a bcd.Separa en cantidad de dígitos y convierte.
{
	uint8_t i;
		for ( i=digits;i>0;i--)
		{
			bcd_number [i-1]= data%10; // Asigno el resto a la dirección donde apunta el puntero.
			data = data/10;
			//printf(" %d \r\n",*bcd_number);//muestra el resto por pantalla.
		}
}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	/* initializations */
	SystemClockInit();
	uint32_t data=124;
	uint8_t digits=3;
	uint8_t bcd[3];
	//uint8_t *bcd_ptr;
	//bcd_ptr=bcd;
	

 // while(1)
//    {
		/* main loop */
    	BinaryToBcd(data,digits,bcd);


//	}
    



	/* program should never reach this line */
	return 0;
}
