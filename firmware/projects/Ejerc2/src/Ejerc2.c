/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 * ! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog

 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 10/03/2022 | Document creation		                         |
 * |			|							                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA INÉS NUÑEZ
 *
 */


/*==================[inclusions]=============================================*/
#include "../inc/Ejerc2.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000

/*==================[internal data definition]===============================*/
void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	/* initializations */
	uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	//LedOn(LED_3);
	
    while(1)
    {
    	/* main loop */
    	teclas  = SwitchesRead();
    	switch(teclas)
    	{
    		case SWITCH_2:
    			LedOn(LED_1);
				Delay();
				LedOff(LED_1);
				Delay();
			break;
    	    case SWITCH_3:
    	    	LedOn(LED_2);
    	    	Delay();
    	    	LedOff(LED_2);
    	    	Delay();
    	    break;
    	    case SWITCH_4:
    	    	LedOn(LED_3);
    	        Delay();
    	        LedOff(LED_3);
    	        Delay();
    	     break;

    	    case SWITCH_1:
    	    	teclas  = SwitchesRead();
    	    	switch(teclas)
    	    	{
    	     		case SWITCH_2:
    	    		LedOn(LED_RGB_B);
    	    		Delay();
    	    		LedOff(LED_RGB_B);
    	    		Delay();
    	    		break;
    	    		case SWITCH_3:
    	        	LedOn(LED_RGB_R);
    	        	Delay();
    	        	LedOff(LED_RGB_R);
    	        	Delay();
    	        	break;
    	     		case SWITCH_4:
    	        	LedOn(LED_RGB_G);
    	        	Delay();
    	        	LedOff(LED_RGB_G);
    	        	Delay();
    	        	break;
    	    	}
    	     break;

    	   }
    	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

