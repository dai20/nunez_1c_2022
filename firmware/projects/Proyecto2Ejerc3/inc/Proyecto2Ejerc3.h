/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA INÉS NUÑEZ
*/

/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 *  El programa mide la distancia por ultrasonido, emplea interrupciones y
 *  manda los datos medidos por puerto serie.
 *  También controla la EDU-CIAA:
 *  - Presionando la tecla 'o' se activa/desactiva la medición.
 *  - Presionando la tecla 'h' se pausa/despausa la medición.
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/04/2022 | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |         						 |
 *
 * @author DAIANA INÉS NUÑEZ
 *
 */

#ifndef _PROYECTO2EJERC3_H
#define _PROYECTO2EJERC3_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[macros and definitions]=================================*/

/*==================[typedef]================================================*/

/*==================[internal functions declaration]=========================*/
/**
* @fn uint16_t measure(void)
* @brief Mide la distancia en cm y retorna dicho valor.
* @return measureCm, que indica el valor medido en cm.
*/
uint16_t measure(void);

/**
* @fn void turnOnLed (uint16_t measuredValue)
* @brief Enciende el/los Led/s según el valor de distancia medido.
* @param[in] measuredValue, el valor medido.
* @return null.
*/
void turnOnLed (uint16_t measuredValue);

/**
* @fn void Tecla1myInterruption(void)
* @brief Modifica el estado de la bandera lighter_flg, que Activa/desactiva el bloque de lectura y muestra.
* @return null.
*/
void Tecla1myInterruption(void);

/**
* @fn void Tecla2myInterruption(void)
* @brief Modifica el estado de la bandera hold_flg, que Activa/desactiva el bloque de pausa.
* @return null.
*/
void Tecla2myInterruption(void);

/**
* @fn void TimermyInterruption(void)
* @brief Modifica el estado de la bandera timer_flg.
* @return null.
*/
void TimermyInterruption(void);

/**
* @fn void UartInterruption(void)
* @brief Según la tecla preionada 'o' u 'h', modifica el estado de la bandera lighter_flg o hold_flg, respectivamente.
* A la vez que enciende el LED_1 o LED_2.
* @return null.
*/
void UartInterruption(void);
/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PROYECTO2EJERC3_H */

