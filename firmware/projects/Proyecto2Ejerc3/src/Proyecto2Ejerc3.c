/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA INÉS NUÑEZ
*/
/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * |21/04/2022  | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA INÉS NUÑEZ
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2Ejerc3.h"       /* <= own header */

#include "hc_sr04.h"
#include "lcditse0803.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

bool lighter_flg=0;//ACTIVA o DETIENE la medición.
bool hold_flg= 0;//Mantiene el rtdo., PAUSA la medición.
bool timer_flg=0;

/*==================[internal functions declaration]=========================*/
//INTERRUPCIONES:
void Tecla1myInterruption(void) //Tecla1= SWITCH_1, Activa/Detiene.
{
	lighter_flg =! lighter_flg;
}


void Tecla2myInterruption(void)//Tecla2= SWITCH_2, Pausa.
{
	hold_flg =! hold_flg;
}


void TimermyInterruption(void)
{
	timer_flg=! timer_flg;
}


void UartInterruption(void)
{
	uint8_t data;
	UartReadByte(SERIAL_PORT_PC, &data);
	switch(data)
	{
		case 'o':
			LedToggle(LED_1);
			lighter_flg =! lighter_flg; //interrupción de la tecla 1.
			break;

		case 'h':
			LedToggle(LED_2);
			hold_flg =! hold_flg;////interrupción de la tecla 2.
			break;
	}
}
//---

uint16_t measure(void)//Mide en cm y me devuelve dicho valor.
{
	uint16_t measureCm;
	measureCm=HcSr04ReadDistanceInCentimeters();// Valor medido en cm.
	return measureCm;
}

void turnOnLed (uint16_t measuredValue)//Enciende los Leds según la distancia.
{
	if(0<= measuredValue && measuredValue<=10)
	{
		LedOn(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	else if (10<measuredValue && measuredValue<=20)
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	else if (20<measuredValue && measuredValue<=30)
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
	}
	else
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
	}
}



/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	/* initializations */
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	LcdItsE0803Init();
	HcSr04Init(GPIO_T_COL0, GPIO_T_FIL0);// Inicializa los pines del Sensor de Ultrasonido. Puedo elegir cualquiera de los color naranja.
				//Disposición de los parámetros: gpio_t echo, gpio_t trigger.

	uint16_t medidaenCm;
	uint8_t value;

	timer_config timer_myStructure= {TIMER_A,
									1000,
									TimermyInterruption};

	 serial_config uart_myStructure= {SERIAL_PORT_PC,
			 	 	 	 	 	 	 115200,
									 UartInterruption};

	SwitchActivInt(SWITCH_1 , Tecla1myInterruption);//Inicializo las interrupciones por tecla.
	SwitchActivInt(SWITCH_2 , Tecla2myInterruption);

	TimerInit(&timer_myStructure);//Inicializo la interrupción por timer.
	TimerStart(timer_myStructure.timer);//Enciendo el timer.

	UartInit(&uart_myStructure);
	
    while(1){
		/* main loop */
    	if(timer_flg)
    	{
    		if(lighter_flg)// Activa, encendido.
    		{
    			if (hold_flg)
    			{
    				medidaenCm= measure();
    			 }

    			else
    			{
    				medidaenCm= measure();//paso el valor de la medición.
    				turnOnLed (medidaenCm);//enciendo los leds correspondientes.
    				LcdItsE0803Write(medidaenCm);//En lugar de ShowDisPlay(data, digits, port_arrLcd, array_lcdOutput);
    				//uint8_t array[]={0,0,0,' ','c','m','\r','\n'};	// No debo agregar el \0.

    				UartSendString(SERIAL_PORT_PC, UartItoa(medidaenCm, 10));
    				UartSendString(SERIAL_PORT_PC, " cm\r\n");

    				//	UartSendString(uart_myStructure.port, &UartReadByte(SERIAL_PORT_PC, &value));
    			}

    			timer_flg=! timer_flg;
    		}

    		else
    		{
    			LedOff(LED_RGB_R);
    			LedOff(LED_RGB_G);
    			LedOff(LED_RGB_B);
    			LedOff(LED_1);
    			LedOff(LED_2);
    			LedOff(LED_3);
    			medidaenCm=0;
    			LcdItsE0803Write(medidaenCm);
    		}

    	}
    }
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

