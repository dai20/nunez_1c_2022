/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA INÉS NUÑEZ
*/

/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * El programa recibe un dígito en código BCD y establece en qué posición de un vector colocar cada bit del
 * dígito BCD . Además indexa dicho vector, para operar sobre el puerto
 * y pin que corresponda.
 *
 * @section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 17/03/2022 | Document creation		                         |
 * |			| 							                     |
 * | 			| 	                     						 |         						 |
 *
 * @author DAIANA INÉS NUÑEZ
 *
 */

#ifndef _EJERC5_H
#define _EJERC5_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[macros and definitions]=================================*/

/*==================[typedef]================================================*/

/*==================[internal functions declaration]=========================*/

/**
@fn void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number)
@brief Separa en cantidad de dígitos un nro. Decimal y
Convierte cada dígito Decimal a BCD.
@param[in] el nro. decimal a convertir.
@param[in] la cantidad de dígitos de salida.
@param[in] un puntero a un arreglo donde se almacenan los n dígitos.
 **/
void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number);

/**
@fn void BcdGpio(uint8_t bcd, gpioConf_t *portPtr)
@brief Asigna los bits de un dígito en código BCD a un vector. Además indexa dicho vector,
para operar sobre el puerto y pin que corresponda.
@param[in] un dígito en código BCD.
@param[in] un puntero a una estructura del tipo gpioConf_t.
 **/
void BcdGpio(uint8_t bcd, gpioConf_t *portPtr);

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EJERC5_H */

