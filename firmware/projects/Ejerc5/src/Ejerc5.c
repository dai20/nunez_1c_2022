/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*
*
* Initials     Name
* ---------------------------
* DN		DAIANA INÉS NUÑEZ
*
*/
 /* ! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 17/03/2022 | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA INÉS NUÑEZ
 */

/*==================[inclusions]=============================================*/
#include "../inc/Ejerc5.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include <stdio.h>
#include <stdint.h> //se utiliza para definir los tamaños de tipos enteros.

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
typedef struct
{
	gpio_t pin;				/*!< GPIO pin number */
	io_t dir;				/*!< GPIO direction '0' IN;  '1' OUT */
} gpioConf_t;


/*==================[internal functions declaration]=========================*/

void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number)// Decimal a bcd.Separa en cantidad de dígitos y convierte.
{
	uint8_t i;

	for ( i=digits;i>0;i--)
	{
		bcd_number [i-1]= data%10; // Asigno el resto a la dirección donde apunta el puntero.
		data = data/10;
		//printf(" %d \r\n",*bcd_number);//muestra el resto por pantalla.
	}
}


void BcdGpio(uint8_t bcd, gpioConf_t *portPtr)
{
	uint8_t i, j;

	for(i=0;i<4;i++)
	{
		GPIOInit ( portPtr[i].pin,  portPtr[i].dir);
	};

	for (j=0; j<4; j++)
	{
   	GPIOState(portPtr[j].pin, ((bcd&(1<<j))>>j));
	}
}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	/* initializations */
	SystemClockInit();
	uint32_t data=124;
	uint8_t digits=3;
	uint8_t bcd1[3];

	gpioConf_t port_arrLcd[4]= {{GPIO_LCD_1, GPIO_OUTPUT},
								{GPIO_LCD_2, GPIO_OUTPUT},
								{GPIO_LCD_3, GPIO_OUTPUT},
								{GPIO_LCD_4, GPIO_OUTPUT}};

	//while(1){
	//		/* main loop */
	BinaryToBcd(data,digits,bcd1);

//	for (int i=0;i<digits;i++)
//		{
//			bcd1[i]=*bcd_ptr;
			//printf("Digito % \n", digits);
			//	printf("%i\n", *bcd_ptr);
//			bcd_ptr++;
//		}

	for (int i=0;i<digits;i++)
	{
		BcdGpio(bcd1[i], port_arrLcd);
	}
//	}


	
   	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

