/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA INÉS NUÑEZ
*/
/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * 	El programa digitaliza una señal analógica y la transmite a un graficador de puerto serie de la PC.
 * 	La señal ingresa por el canal CH1 del converos AD.
 * 	La señal se transmite por UART, conectada al puerto serie de la PC.
 * 	La frecuencia de muestreo (fm) es de 500hz, o sea T=2 mseg.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/05/2020 | Document creation		                         |
 * | 			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA INÉS NUÑEZ
 *
 */

#ifndef _PROYECTO2EJERC4_H
#define _PROYECTO2EJERC4_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[macros and definitions]=================================*/

/*==================[typedef]================================================*/

/*==================[internal functions declaration]=========================*/

/**
* @fn void ADCinterruption(void)
* @brief Lee los datos por canal CH1, los convierte a decimal y los muestra por PC.
* @return null
*/
void ADCinterruption(void);

/**
* @fn void TimerAInterruption(void)
* @brief Convierte los datos de formato analógico a digital..
* @return null
*/
void TimerAInterruption(void);

/**
* @fn void DACInterruption (void)
* @brief Recorre y muestra el vector de ecg.
* @return null
*/
void DACInterruption (void);


/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PROYECTO2EJERC4_H */

