/* Copyright 2018,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA INÉS NUÑEZ
*/
/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/05/2020 | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA INÉS NUÑEZ
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2Ejerc4.h"       /* <= own header */

#include "hc_sr04.h"
#include "lcditse0803.h"
#include "led.h"
#include "switch.h"
#include "analog_io.h"
#include "gpio.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

bool timer_flg=0;
uint16_t value1;
uint8_t cont=0;

#define BUFFER_SIZE 231

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};
/*==================[internal functions declaration]=========================*/

void ADCinterruption(void)//LEE Y MUESTRA
{
	AnalogInputRead(CH1, &value1);//es la variable por referecina &value donde quiero que guarde el dato.
	//value= ecg[cont];
	UartSendString(SERIAL_PORT_PC, UartItoa(value1, 10));
	UartSendString(SERIAL_PORT_PC, "\r");
}

void TimerAInterruption(void)//INTERRUPCIÓN DEL TIMER A.
{
	AnalogStartConvertion();
}


void DACInterruption (void)//INTERRUPTION DEL TIMER B.
{
	AnalogOutputWrite(ecg[cont]);//ponerMuestraDAC ()
	if(cont<BUFFER_SIZE)// Si el contador es menor a 231.
	{
		cont++;
	}
	else
	{
		cont=0;
	}

}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	/* initializations */
	SystemClockInit();
	//LedsInit();
	//SwitchesInit();
	//LcdItsE0803Init();
	//HcSr04Init(GPIO_T_COL0, GPIO_T_FIL0);// Inicializa los pines del Sensor de Ultrasonido. Puedo elegir cualquiera de los color naranja.
					//Disposición de los parámetros: gpio_t echo, gpio_t trigger.

	//uint16_t medidaenCm;
	//uint8_t value;

	timer_config timer_myStructure_lee_fm= {TIMER_A,
											2,
											TimerAInterruption
											};

	timer_config timer_myStructure_generaECG= {TIMER_B,
												4,
												DACInterruption};

	serial_config uart_myStructure= {SERIAL_PORT_PC,
				 	 	 	 	 	 115200,//HAY QUE CALCULARLO
									 NO_INT};//Cuando no uso la función de interrupción.

	//INICIALIZO la estructura del ADC
	analog_input_config analog_myStructureInput= {CH1,
			 	 	 	 	 	 	 	 	 	 AINPUTS_SINGLE_READ,
												 ADCinterruption};// AnalogStartConvertion()
	//INICIALIZO la estructura del DAC
	//analog_input_config analog_myStructureOuput= {CH0, //DAC
				 	 	 	 	 	 	 	 	// AINPUTS_SINGLE_READ,
												// ponerMuestraDAC};

	TimerInit(&timer_myStructure_lee_fm);//Inicializo la interrupción por timer A.
	TimerStart(timer_myStructure_lee_fm.timer);
	TimerInit(&timer_myStructure_generaECG);//Inicializo la interrupción por timer B.
	TimerStart(timer_myStructure_generaECG.timer);

	UartInit(&uart_myStructure);

	AnalogInputInit(&analog_myStructureInput);

	AnalogOutputInit();
	
	AnalogStartConvertion();

    while(1){
		/* main loop */

	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

