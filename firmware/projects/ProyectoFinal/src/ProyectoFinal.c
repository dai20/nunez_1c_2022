/* Copyright 2022,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
* Initials     Name
* ---------------------------
* DN		DAIANA NUÑEZ
*
*
*
* @section changelog Changelog
*
* |   Date	    | Description                                    |
* |:----------:	|:-----------------------------------------------|
* | 10/05/2022	| Document creation		                         |
* |				| A functionality is added	                     |
* | 			| 	                     						 |
*
* @author Daiana Nuñez
*
*/

/*==================[inclusions]=============================================*/
#include "../inc/ProyectoFinal.h"       /* <= own header */

//Driver Devices:
#include "Balance_CD_30.h"
#include "buzzer.h"
#include "hc_sr04.h"
//#include "led.h"
#include "Servo_SG90.h"


//Driver Microcontroller:
#include "analog_io.h"
#include "gpio.h"
#include "sapi_rtc.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"

#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

#define PI 3.14159f

/*==================[typedef]================================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

uint8_t  mes, dia, hora,  minuto, segundo;	 /* 0 to 59   */
uint8_t cantidad;
uint16_t racion;

float cteCalculoVolumen= 33501/400; //cte. usada en el cálculo de volumen del cono truncado.
										//cte=(RMayor^2 + Rmenor^2 + RMayorRmenor).
										//VconoTruncado= (H*PI)/3*(cte).
uint16_t volumenMinimo= (11167/40)*PI;//Volumen mínimo 877.0541291 cm3.
uint16_t volumenSensado, msb1, lsb1;

rtc_t hora_config;
mascota generico;

//typedef struct {
  // uint16_t year;	 /* 1 to 4095 */
 //  uint8_t  month; /* 1 to 12   */
 //  uint8_t  mday;	 /* 1 to 31   */
 //  uint8_t  wday;	 /* 1 to 7    */
 //  uint8_t  hour;	 /* 0 to 23   */
//   uint8_t  min;	 /* 0 to 59   */
//   uint8_t  sec;	 /* 0 to 59   */
//} rtc_t;

/*
 *typedef struct {
	uint8_t tipo;
	uint8_t meses;
	uint16_t peso;
	uint8_t condicion;
	uint16_t comidaDiaria;
	uint8_t cantidadDeRaciones;

} mascota;
 */

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

// MENU Y OBTENCIÓN DE DATOS por Uart:
void cargarDatos (void)
{
	//Cargo estructura rtc_t:
	UartSendString(SERIAL_PORT_PC, "Ingrese  día: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &dia); //( Puerto que se desea leer, Puntero a la variable donde se desea almacenar el valor leído.)
	hora_config.mday = dia;
	UartSendString(SERIAL_PORT_PC, "Ingrese mes: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &mes);
	hora_config.month = mes;

	UartSendString(SERIAL_PORT_PC, "Ingrese el año: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &msb1);
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &lsb1);
	hora_config.year=((msb1<<8)|lsb1);

	UartSendString(SERIAL_PORT_PC, "Ingrese hora: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &hora_config.hour);

	UartSendString(SERIAL_PORT_PC, "Ingrese minuto: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &hora_config.min);

	UartSendString(SERIAL_PORT_PC, "Ingrese segundo: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &hora_config.sec);
	rtcConfig(&hora_config);// Funciona similar a una fción de inicialización.
	rtcWrite(&hora_config);

	//Cargo estructura mascota:
	UartSendString(SERIAL_PORT_PC, "Ingrese tipo de mascota: a. Para gato. b. Para perro \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &generico.tipo);
	UartSendString(SERIAL_PORT_PC, "Ingrese edad en meses de la mascota: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &generico.meses);

	UartSendString(SERIAL_PORT_PC, "Ingrese el peso de la mascota en gramos: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &msb1);
	//UartReadByte(SERIAL_PORT_PC, &isb);
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &lsb1);
	generico.peso=((msb1<<8)|lsb1);

	UartSendString(SERIAL_PORT_PC, "Ingrese condición física de la mascota:\r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, "Si tiene Sobrepeso o no sale del hogar, ingrese 0 \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, "Si realiza paseos ocacionales de no más de 1hr, ingrese 1 \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, "Si realiza paseos de no más de 1:30hr, ingrese 2 \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, "Si posee bajo peso o realiza paseos/entrenamiento de más de 1:30hr, ingrese 3 \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	while(!UartRxReady(SERIAL_PORT_PC)){};
	UartReadByte(SERIAL_PORT_PC, &generico.condicion);
}

void calcularCantidadTotal(mascota *generico)//Cantidad total de comida.
{
	switch (generico->tipo)
	{
		case 'a'://Gato
			if(generico->meses<=12)
			{
				if(generico->meses<=4)
				{
					generico->comidaDiaria=100; //100gr.
				}
				if(generico->meses>4 && generico->meses<=6)
				{
					generico->comidaDiaria=120;
				}
				if(generico->meses>6 && generico->meses<=8)
				{
					generico->comidaDiaria=150;
				}
				else
				{
					switch (generico->condicion)
					{
						case 0:
							generico->comidaDiaria = 130;
						break;
						case 1:
							generico->comidaDiaria = 130;
						break;
						case 2:
							generico->comidaDiaria = 140;
						break;
						case 3:
							generico->comidaDiaria = 150;
						break;
					}
				}
			}

			else //Si es gato y mayor a 12 meses.
			{
			if (generico->peso <= 1000)
			{
				switch (generico->condicion){
					case 0:
						generico->comidaDiaria = 25;
					break;
					case 1:
						generico->comidaDiaria = 28;
					break;
					case 2:
						generico->comidaDiaria = 30;
					break;
					case 3:
						generico->comidaDiaria = 40;
					break;
				}
			}
			if (generico->peso> 1000 && generico->peso <= 2000)
			{
				switch (generico->condicion){
					case 0:
						generico->comidaDiaria = 30;
					break;
					case 1:
						generico->comidaDiaria = 32;
					break;
					case 2:
						generico->comidaDiaria = 35;
					break;
					case 3:
						generico->comidaDiaria = 45;
					break;
				}
			}
			if (generico->peso> 2000 && generico->peso <= 3000)
			{
				switch (generico->condicion){
					case 0:
						generico->comidaDiaria = 40;
					break;
					case 1:
						generico->comidaDiaria = 42;
					break;
					case 2:
						generico->comidaDiaria = 45;
					break;
					case 3:
						generico->comidaDiaria = 55;
					break;
				}
			}
			if (generico->peso> 3000 && generico->peso <= 4000)
			{
				switch (generico->condicion){
					case 0:
						generico->comidaDiaria = 50;
					break;
					case 1:
						generico->comidaDiaria = 52;
					break;
					case 2:
						generico->comidaDiaria = 55;
					break;
					case 3:
						generico->comidaDiaria = 65;
					break;
				}
			}
			if (generico->peso> 4000 && generico->peso <= 5000)
			{
				switch (generico->condicion){
					case 0:
						generico->comidaDiaria = 60;
					break;
					case 1:
						generico->comidaDiaria = 62;
					break;
					case 2:
						generico->comidaDiaria = 65;
					break;
					case 3:
						generico->comidaDiaria = 75;
					break;
				}
			}
			if (generico->peso> 5000 && generico->peso <= 6000)
			{
				switch (generico->condicion){
					case 0:
						generico->comidaDiaria = 60;
					break;
					case 1:
						generico->comidaDiaria = 66;
					break;
					case 2:
						generico->comidaDiaria = 90;
					break;
					case 3:
						generico->comidaDiaria = 120;
					break;
				}
			}
			if (generico->peso> 6000 && generico->peso <= 7000)
			{
				switch (generico->condicion){
					case 0:
						generico->comidaDiaria = 70;
					break;
					case 1:
						generico->comidaDiaria = 77;
					break;
					case 2:
						generico->comidaDiaria = 105;
					break;
					case 3:
						generico->comidaDiaria = 140;
					break;
				}
			}
			if (generico->peso> 7000 && generico->peso <= 8000)
			{
				switch (generico->condicion){
					case 0:
						generico->comidaDiaria = 80;
					break;
					case 1:
						generico->comidaDiaria = 88;
					break;
					case 2:
						generico->comidaDiaria = 120;
					break;
					case 3:
						generico->comidaDiaria = 160;
					break;
				}
			}
			else //Si es gato mayor de 12 meses y pesa más de 8kg.
			{
				switch (generico->condicion){
					case 0:
						generico->comidaDiaria = 10*generico->peso;
					break;
					case 1:
						generico->comidaDiaria = 12*generico->peso;
					break;
					case 2:
						generico->comidaDiaria = 15*generico->peso;
					break;
					case 3:
						generico->comidaDiaria = 20*generico->peso;
					break;
				}
			}

		}
		break;


		case 'b': //PERRO
				if (generico->meses <= 12)
				{
					if(generico->peso<=3000) //PERRO MINI
					{
						if (generico->meses <= 4)
						{
							generico->comidaDiaria = 30*generico->peso;
						}
						if (generico->meses > 4 && generico->meses <= 5) {
							generico->comidaDiaria = 25*generico->peso;
						}
						if (generico->meses > 5 && generico->meses <= 8) {
							switch (generico->condicion){
								case 0:
									generico->comidaDiaria = 20*generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25*generico->peso;
								break;
								case 2:
									generico->comidaDiaria = 30*generico->peso;
								break;
								case 3:
									generico->comidaDiaria = 32*generico->peso;
								break;
							}
						}
						if (generico->meses > 8 && generico->meses <= 12)
						{
							switch (generico->condicion) {
								case 0:
									generico->comidaDiaria = 15*generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25*generico->peso;
									break;
								case 2:
									generico->comidaDiaria = 30*generico->peso;
									break;
								case 3:
									generico->comidaDiaria = 32*generico->peso;
								break;
							}
						}
					}

					if(generico->peso>3000 && generico->peso<=9000)//PERRO PEQUEÑO
					{
						if (generico->meses <= 4)
						{
							generico->comidaDiaria = 25*generico->peso;
						}
						if (generico->meses > 4 && generico->meses <= 5) {
							generico->comidaDiaria = 20*generico->peso;
						}
						if (generico->meses > 5 && generico->meses <= 8) {
							switch (generico->condicion){
								case 0:
									generico->comidaDiaria = 20*generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25*generico->peso;
								break;
								case 2:
									generico->comidaDiaria = 30*generico->peso;
								break;
								case 3:
									generico->comidaDiaria = 32*generico->peso;
								break;
							}
						}
						if (generico->meses > 8 && generico->meses <= 12)
						{
							switch (generico->condicion) {
								case 0:
									generico->comidaDiaria = 15 * generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25 * generico->peso;
									break;
								case 2:
									generico->comidaDiaria = 30 * generico->peso;
									break;
								case 3:
									generico->comidaDiaria = 32 * generico->peso;
								break;
							}
						}
					}

					if(generico->peso>9000 && generico->peso<=15000)//PERRO MEDIANO
					{
						if (generico->meses <= 4)
						{
							generico->comidaDiaria = 25*generico->peso;
						}
						if (generico->meses > 4 && generico->meses <= 5){
							generico->comidaDiaria = 20*generico->peso;
						}
						if (generico->meses > 5 && generico->meses <= 8) {
							switch (generico->condicion){
								case 0:
									generico->comidaDiaria = 20*generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25*generico->peso;
								break;
								case 2:
									generico->comidaDiaria = 30*generico->peso;
								break;
								case 3:
									generico->comidaDiaria = 32*generico->peso;
								break;
							}
						}
						if (generico->meses > 8 && generico->meses <= 12)
						{
							switch (generico->condicion) {
								case 0:
									generico->comidaDiaria = 15 * generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25 * generico->peso;
									break;
								case 2:
									generico->comidaDiaria = 30 * generico->peso;
									break;
								case 3:
									generico->comidaDiaria = 32 * generico->peso;
								break;
							}
						}
					}

					if(generico->peso>15000 && generico->peso<=25000)//PERRO MEDIANO GRANDE
					{
						if (generico->meses <= 4)
						{
							generico->comidaDiaria = 25*generico->peso;
						}
						if (generico->meses > 4 && generico->meses <= 5) {
							generico->comidaDiaria = 20*generico->peso;
						}
						if (generico->meses > 5 && generico->meses <= 8) {
							switch (generico->condicion){
								case 0:
									generico->comidaDiaria = 20*generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25*generico->peso;
								break;
								case 2:
									generico->comidaDiaria = 30*generico->peso;
								break;
								case 3:
									generico->comidaDiaria = 32*generico->peso;
								break;
							}
						}
						if (generico->meses > 8 && generico->meses <= 12)
						{
							switch (generico->condicion) {
								case 0:
									generico->comidaDiaria = 15 * generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25 * generico->peso;
									break;
								case 2:
									generico->comidaDiaria = 30 * generico->peso;
									break;
								case 3:
									generico->comidaDiaria = 32 *generico->peso;
								break;
							}
						}
					}

					if(generico->peso>25000 && generico->peso<=35000)//PERRO GRANDE
					{
						if (generico->meses <= 4)
						{
							generico->comidaDiaria = 20*generico->peso;
						}
						if (generico->meses > 4 && generico->meses <= 5) {
							generico->comidaDiaria = 15*generico->peso;
						}
						if (generico->meses > 5 && generico->meses <= 8) {
							switch (generico->condicion){
								case 0:
									generico->comidaDiaria = 10*generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25*generico->peso;
								break;
								case 2:
									generico->comidaDiaria = 30*generico->peso;
								break;
								case 3:
									generico->comidaDiaria = 32*generico->peso;
								break;
							}
						}
						if (generico->meses > 8 && generico->meses <= 12)
						{
							switch (generico->condicion) {
								case 0:
									generico->comidaDiaria = 10 * generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25 * generico->peso;
									break;
								case 2:
									generico->comidaDiaria = 30 *generico->peso;
									break;
								case 3:
									generico->comidaDiaria = 32 * generico->peso;
								break;
							}
						}
					}

					if(generico->peso>35000 && generico->peso<=45000)//PERRO EXTRA GRANDE
					{
						if (generico->meses <= 4)
						{
							generico->comidaDiaria = 20*generico->peso;
						}
						if (generico->meses > 4 && generico->meses <= 5) {
							generico->comidaDiaria = 15*generico->peso;
						}
						if (generico->meses > 5 && generico->meses <= 8) {
							switch (generico->condicion){
								case 0:
									generico->comidaDiaria = 10*generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25*generico->peso;
								break;
								case 2:
									generico->comidaDiaria = 30*generico->peso;
								break;
								case 3:
									generico->comidaDiaria = 32*generico->peso;
								break;
							}
						}
						if (generico->meses > 8 && generico->meses <= 12)
						{
							switch (generico->condicion) {
								case 0:
									generico->comidaDiaria = 10 * generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25 * generico->peso;
									break;
								case 2:
									generico->comidaDiaria = 30 * generico->peso;
									break;
								case 3:
									generico->comidaDiaria = 32 * generico->peso;
								break;
							}
						}
					}

					if(generico->peso>35000 && generico->peso<=45000)//PERRO GIGANTE
					{
						if (generico->meses <= 4)
						{
							generico->comidaDiaria = 20*generico->peso;
						}
						if (generico->meses> 4 && generico->meses <= 5) {
							generico->comidaDiaria = 15*generico->peso;
						}
						if (generico->meses > 5 && generico->meses <= 8) {
							switch (generico->condicion){
								case 0:
									generico->comidaDiaria = 10*generico->peso;
								break;
								case 1:
									generico->comidaDiaria = 25*generico->peso;
								break;
								case 2:
									generico->comidaDiaria = 30*generico->peso;
								break;
								case 3:
									generico->comidaDiaria = 32*generico->peso;
								break;
							}
						}
						if (generico->meses> 8 && generico->meses <= 12)
						{
							switch (generico->condicion) {
							case 0:
								generico->comidaDiaria = 10 * generico->peso;
							break;
							case 1:
								generico->comidaDiaria = 25 * generico->peso;
								break;
							case 2:
								generico->comidaDiaria = 30 * generico->peso;
								break;
							case 3:
								generico->comidaDiaria = 32 * generico->peso;
							break;
							}
						}
					}
				}

				else//Si es perro mayor a 12 meses.
				{
					if (generico->peso <= 15000) //PERRO MINI, PERRO PEQUEÑO y PERRO MEDIANO.
					{
						switch (generico->condicion) {
							case 0:
								generico->comidaDiaria = 15 * generico->peso;
							break;
							case 1:
								generico->comidaDiaria = 25 *generico->peso;
							break;
							case 2:
								generico->comidaDiaria = 30 * generico->peso;
							break;
							case 3:
								generico->comidaDiaria = 32 * generico->peso;
							break;
						}
					}

					else//PERRO MEDIANO GRANDE, PERRO GRANDE y PERRO EXTRA GRANDE.
					{
						switch (generico->condicion) {
							case 0:
								generico->comidaDiaria = 10 * generico->peso;
							break;
							case 1:
								generico->comidaDiaria = 25 * generico->peso;
							break;
							case 2:
								generico->comidaDiaria = 30 * generico->peso;
							break;
							case 3:
								generico->comidaDiaria = 32 * generico->peso;
							break;
						}
					}
				}
			break;
		}
}

uint16_t calcularRaciones(mascota *generico)//Calcula el peso en gramos de c/d racion respecto del total de comida diaria.
{
	uint16_t gramosPorRacion;
	switch (generico->tipo)
	{
		case 'a':
			if(generico->meses<=12)
			{
				generico->cantidadDeRaciones=5;
			}
			else
			{
				generico->cantidadDeRaciones=2;
			}
		break;
		case 'b':
			if(generico->meses<=12)
			{
				generico->cantidadDeRaciones=4;
			}
			else
			{
				generico->cantidadDeRaciones=2;
			}
		break;
	}

	gramosPorRacion= (uint16_t)(generico->comidaDiaria/generico->cantidadDeRaciones);

	return (gramosPorRacion);
}


//INTERRUPCIONES y FUNCIONES ASOCIADAS:

void sensarVolumenAgua(void)//Calcula el volumen de agua que posee el recipiente.
{
	uint16_t distanciaMedida;
	distanciaMedida=HcSr04ReadDistanceInCentimeters();
	volumenSensado=(((float)distanciaMedida)*PI/3)*cteCalculoVolumen;
}

void TimerAInterruption(void)//INTERRUPCIÓN DEL TIMER A.
{
	rtcRead(&hora_config);

	if (hora!= hora_config.hour)
	{
		sensarVolumenAgua();

		if (volumenSensado > volumenMinimo)
		{
			BuzzerOn();


		} else
		{
			BuzzerOff();
		}

		hora = hora_config.hour;
	}
}


//--------

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void){
	/* initializations */
	SystemClockInit();
	//LedsInit();

	HcSr04Init(GPIO_T_COL0, GPIO_T_FIL0);// Inicializa los pines del Sensor de Ultrasonido. Puedo elegir cualquiera de los color naranja.
						//Disposición de los parámetros: gpio_t echo, gpio_t trigger.
	BalanceCD30Init();
	ServoSG90Init();
	ServoSG90Move(0);
	BuzzerInit();
	BuzzerOff();
	

	//Estructura, inicialización y encendido del timer A:
	timer_config timer_myStructure= {TIMER_A,
									2000,
									TimerAInterruption
									};

	TimerInit(&timer_myStructure);//Inicializo la interrupción por timer A.


	//Estructura e inicialización de la Uart:
	serial_config uart_myStructure= {SERIAL_PORT_PC,
						 	 	 	115200,//HAY QUE CALCULARLO
									NO_INT};//Cuando no uso la función de interrupción.
	UartInit(&uart_myStructure);

	//Estructura e inicialización del ADC:
	analog_input_config analog_myStructureInput= {CH1,
					 	 	 	 	 	 	 	AINPUTS_SINGLE_READ,
												NULL};// AnalogStartConvertion()
	AnalogInputInit(&analog_myStructureInput);
	AnalogOutputInit();//DAC. -Inicialización de salida analógica.
	AnalogStartConvertion();

	cargarDatos ();
	/*
	hora_config.mday=1;
	hora_config.month=1;
	hora_config.year=2022;
	hora_config.hour=8;
	hora_config.wday=1;
	hora_config.min=10;
	hora_config.sec=11;

	generico.tipo= 'a';
	generico.meses=11;
	generico.peso=3000;
	generico.condicion=3;
	rtcConfig(&hora_config);// Funciona similar a una fción de inicialización.
	rtcWrite(&hora_config);

*/
	TimerStart(timer_myStructure.timer);
	/*
	//Cargo estructura rtc_t:
	UartSendString(SERIAL_PORT_PC, "El día ingrasado es: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &hora_config.mday); //( Puerto que se desea leer, Puntero a la variable donde se desea almacenar el valor leído.)

	UartSendString(SERIAL_PORT_PC, "El mes ingresado es: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &hora_config.month);

	UartSendString(SERIAL_PORT_PC, "El año ingresado es: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &hora_config.year);

	UartSendString(SERIAL_PORT_PC, "La hora ingresada es : \r");1
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &hora_config.hour);
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &hora_config.min);
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &hora_config.sec);

		//Cargo estructura mascota:
	UartSendString(SERIAL_PORT_PC, "El tipo de mascota ingresado y sus características son:\r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &generico.tipo);

	UartSendString(SERIAL_PORT_PC, "meses: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &generico.meses);

	UartSendString(SERIAL_PORT_PC, "Peso: \r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &generico.peso);

	UartSendString(SERIAL_PORT_PC, "Condición física de la mascota:\r");
	UartSendString(SERIAL_PORT_PC, "\r");
	UartSendString(SERIAL_PORT_PC, &generico.condicion);
*/
	dia=hora_config.mday;
	hora= hora_config.hour;
	minuto= hora_config.min;
	segundo= hora_config.sec;

	//uint16_t pesoComida=0;//Ej 50gr
 	calcularCantidadTotal(&generico);//1000g
    racion = calcularRaciones(&generico);//100g
    cantidad = generico.cantidadDeRaciones;//2,4,5.

//mascota *ptr;
//ptr->condicion=3;


    while(1){
		/* main loop */


    	if (dia == hora_config.mday)
		{
			if (cantidad > 0)
			{
				if ((hora == hora_config.hour && (minuto+1) == hora_config.min )|| (hora + 6 == hora_config.hour && (minuto+1) == hora_config.min ))
				{
					//Soltar comida al plato:
					ServoSG90Move(45);
					while(BalanceCD30Read()<racion){};
					ServoSG90Move(0);
					cantidad--;
				}
			}
		}

		else
		{
			cantidad = generico.cantidadDeRaciones;
		}

	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

