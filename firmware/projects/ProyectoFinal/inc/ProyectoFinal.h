/* Copyright 2022,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA NUÑEZ
*/
/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 * 
 * El programa:
 * - Sensa el nivel de agua que posee el bebedero de una mascota y acciona una alarma en caso de que este sea insuficiente.
 * - También controla la cantidad de alimento a suministrarle en base al tipo de mascota, su edad, peso y condición física.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 *
 *
 * |  Balanza_CD30		|   EDU-CIAA	|
 * |(Sensor analógico)	|     			|
 * |:------------------:|:--------------|
 * | 	VCC		 		| 	 5V			|
 * | 	OUT		 		| 	CH1			|
 * | 	GND		 		| 	GNDA		|
 *
 * |   Servomotor SG90	|   EDU-CIAA	|
 * |:------------------:|:--------------|
 * | 	PWM	 			| 	GPIO_T_FIL2	|
 * | 	VCC	 			| 	5V			|
 * | 	GND	 			| 	GND			|
 *
 * |   HC-SR04			|   EDU-CIAA	|
 * |:------------------:|:--------------|//HcSr04Init(GPIO_T_COL0, GPIO_T_FIL0);
 * | 	Trig 			|  GPIO_T_FIL0	|
 * |	Echo 			|  GPIO_T_COL0 	|
 * | 	VCC	 			| 	5V			|
 * | 	GND	 			| 	GND			|
 *
 * |   Buzzer			|   EDU-CIAA	|
 * |:------------------:|:--------------|
 * | 	OUT 			| 	GPIO_T_FIL1|
 * | 	GND	 			| 	GND			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/05/2022 | Document creation		                         |
 * | 			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @ Daiana Nuñez
 *
 */

#ifndef _PROYECTOFINAL_H
#define _PROYECTOFINAL_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[macros and definitions]=================================*/

/*==================[typedef]================================================*/
typedef struct {
	uint8_t tipo;
	uint8_t meses;
	uint16_t peso;
	uint8_t condicion;
	uint16_t comidaDiaria;
	uint8_t cantidadDeRaciones;

} mascota;

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


/**
* @fn void cargarDatos (void)
* @brief Interactive menu: reads the data and assigns it to structures of type rtc_t and pet.
* Menu interactivo: lee los datos y los asigna a las estructuras del tipo rtc_t y mascota.
* @return null
*/
void cargarDatos (void);

/**
* @fn void calcularCantidadTotal(mascota *generico)
* @brief Calculate the total amount of daily food.//Calcula la cantidad total de alimento diario.
* @param[in] *generico.
* @return null
*/
void calcularCantidadTotal(mascota *generico);

/**
* @fn uint16_t calcularRaciones(mascota *generico)
* @brief Calculate the weight in grams of c/d ration with respect to the total daily food.
* Calcula el peso en gramos de c/d ración respecto del total de comida diaria.
* @param[in] *generico.
* @return Ration weight in grams.//Peso de Ración en gramos.
*/
uint16_t calcularRaciones(mascota *generico);

/**
* @fn void sensarVolumenAgua(void)
* @brief Calculate the volume of water in the container.//Calcula el volumen de agua que posee el recipiente.
* @return null
*/
void sensarVolumenAgua(void);

/**
* @fn void TimerAInterruption(void)
* @brief Interruption that senses the water level every 1hr.//Interrupción que sensa el nivel de agua cada 1hr.
* @return null
*/
void TimerAInterruption(void);
/*==================[end of file]============================================*/


#endif /* #ifndef _PROYECTOFINAL_H */

