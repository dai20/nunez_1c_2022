/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * El programa corrobora la señal de la placa Arduino, mediante el encendido/apagado del LED_2
 *  de la EDU-CIAA a partir de interrupciones producidas en el pin TCOL_1, en cada flanco ascendente de la señal de reloj.
 *
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

#ifndef _ACTIVIDAD_LECTURADEPUERTOS_CONINTERRUPCIONES_H
#define _ACTIVIDAD_LECTURADEPUERTOS_CONINTERRUPCIONES_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _ACTIVIDAD_LECTURADEPUERTOS_CONINTERRUPCIONES_H */

