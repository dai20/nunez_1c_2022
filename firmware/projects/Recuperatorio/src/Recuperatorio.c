/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Recuperatorio.h"       /* <= own header */

//Driver Devices:
#include "hc_sr04.h"

//Driver Microcontroller
#include "analog_io.h"
#include "gpio.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/
uint8_t humedad;
uint16_t volumenSensadoT1, volumenSensadoT2;

bool medir_flg=0;



/*==================[external functions definition]==========================*/
//INTERRUPCIONES

//1-Interrupción por UART:
void UartInterruption(void)//
{
	uint8_t data;
	UartReadByte(SERIAL_PORT_PC, &data);
	switch(data)
	{
		case 'I':
			medir_flg=1;
			//Iniciar todos los sensores
			//	Humedad
			//Nivelt1
			//NivelT2.
			//Iniciar bomba.
		//Abrir Electrovalv.

		break;

		case 'O':
			medir_flg=0;
			//Apagar todos los sensores
			//	Humedad
			//Nivelt1
			//NivelT2.
			//Apagar bomba.
			//Cerrar Electrovalv.
			break;
	}
}

void TimerAInterruption(void)//INTERRUPCIÓN DEL TIMER A.
{

	volumenSensadoT1= nivelTanqueMedirT1();
	volumenSensadoT2=nivelTanqueMedirT2();
	humedad=medirHumedad();

	UartSendString(SERIAL_PORT_PC, "Tanque 1: ");
	UartSendString(SERIAL_PORT_PC,volumenSensadoT1 );
	UartSendString(SERIAL_PORT_PC, "litros\r\n");

	UartSendString(SERIAL_PORT_PC, "Tanque 2: ");
	UartSendString(SERIAL_PORT_PC,volumenSensadoT2 );
	UartSendString(SERIAL_PORT_PC, "litros\r\n");

	UartSendString(SERIAL_PORT_PC, "Humedad del suelo: ");
	UartSendString(SERIAL_PORT_PC,humedad );
	UartSendString(SERIAL_PORT_PC, "%\r\n);

}


uint8_t medirHumedad ()//Inicializa el sensor de Humedad y mide la humedad.
{
	uint16_t hum;

	AnalogInputInit(&analog_myStructureInput);

	AnalogInputReadPolling(CH1, &value);

	hum=(uint8_t)((float)value*(85/3.3));
	return (hum);
}

void activarBomba(void)
{
	GPIOInit(GPIO_6,  GPIO_OUTPUT);
}

void abrirValvula(void)
{
	GPIOInit(GPIO_5,  GPIO_OUTPUT);
}

void cerrarValvula(void){
	GPIOInit(GPIO_5,  GPIO_INPUT);
}

int main(void){
	/* initializations */
	SystemClockInit();

	//Estructura, inicialización y encendido del timer A:
	timer_config timer_myStructure= {TIMER_A,
									60000,
									TimerAInterruption
										};


	TimerInit(&timer_myStructure);//Inicializo la interrupción por timer A.

	//Estructura e inicialización de la Uart:
		serial_config uart_myStructure= {SERIAL_PORT_PC,
							 	 	 	115200,//HAY QUE CALCULARLO
										NO_INT};//Cuando no uso la función de interrupción.
		UartInit(&uart_myStructure);

//Estructura e inicialización del ADC del sensor de Humedad:
	analog_input_config analog_myStructureInput= {CH1,
						 	 	 	 	 	 	 AINPUTS_SINGLE_READ,
												 medirHumedad};// AnalogStartConvertion()
	AnalogInputInit(&analog_myStructureInput);
	AnalogOutputInit();//DAC. -Inicialización de salida analógica.
	AnalogStartConvertion();
	
	while(1){
		/* main loop */
		if (medir_flg)
		{
			if(volumenSensadoT1>200 && volumenSensadoT2<600){

				while(volumenSensadoT2<900)
				{
					activarBomba();
					volumenSensadoT1= nivelTanqueMedirT1();
					volumenSensadoT2=nivelTanqueMedirT2();
				}

				if(humedad<20 && volumenSensadoT2>50)
				{
					abrirValvula();
				}

				if (humedad>50){
					cerrarValvula();
				}
			}

		}
	}
    
	/* program should never reach this line */
	return 0;
}

/*==================[end of file]============================================*/

