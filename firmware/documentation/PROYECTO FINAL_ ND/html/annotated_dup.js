var annotated_dup =
[
    [ "analog_input_config", "structanalog__input__config.html", "structanalog__input__config" ],
    [ "bit_n", "structbit__n.html", "structbit__n" ],
    [ "bit_send", "structbit__send.html", "structbit__send" ],
    [ "digitalIO", "structdigital_i_o.html", "structdigital_i_o" ],
    [ "Font_t", "struct_font__t.html", "struct_font__t" ],
    [ "mma8451_config_t", "structmma8451__config__t.html", "structmma8451__config__t" ],
    [ "pixel", "structpixel.html", "structpixel" ],
    [ "plot_t", "structplot__t.html", "structplot__t" ],
    [ "Record", "struct_record.html", "struct_record" ],
    [ "rtc_t", "structrtc__t.html", "structrtc__t" ],
    [ "serial_config", "structserial__config.html", "structserial__config" ],
    [ "signal_t", "structsignal__t.html", "structsignal__t" ],
    [ "spiConfig_t", "structspi_config__t.html", "structspi_config__t" ],
    [ "timer_config", "structtimer__config.html", "structtimer__config" ]
];