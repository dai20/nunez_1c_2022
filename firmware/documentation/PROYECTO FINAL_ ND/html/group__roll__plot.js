var group__roll__plot =
[
    [ "plot_t", "structplot__t.html", [
      [ "back_color", "structplot__t.html#a649b9f5d9de03ef2fc6e6431546e469a", null ],
      [ "height", "structplot__t.html#ab8f22708bbe2634f9476cd5a7d8e4497", null ],
      [ "width", "structplot__t.html#a82adb24270cdf01bef893d36f2913fc0", null ],
      [ "x_pos", "structplot__t.html#a1acb7934eed116e48eb1fd39d84f1f29", null ],
      [ "x_scale", "structplot__t.html#ad18a4224c513c3096808edb32dcc2765", null ],
      [ "y_pos", "structplot__t.html#a6abe092e3069eb347f28ad183c633806", null ]
    ] ],
    [ "signal_t", "structsignal__t.html", [
      [ "color", "structsignal__t.html#a1c250d9df690cb15a2b504c39f3aebb6", null ],
      [ "plot", "structsignal__t.html#acd4921f0319654d419cff2eff70e054e", null ],
      [ "x_prev", "structsignal__t.html#a1d8490f4564491d103e8230dcdf2fb90", null ],
      [ "y_offset", "structsignal__t.html#ae81588ea149bc2bd2c6fdf762bd2065e", null ],
      [ "y_prev", "structsignal__t.html#a1239e12c4140cc4b709494ca20ceb83e", null ],
      [ "y_scale", "structsignal__t.html#a984177f82cbf13d931c0987289e0accb", null ]
    ] ],
    [ "RollPlot_Draw", "group__roll__plot.html#gabea6b076c1f8b324f3d85bc8c1d79f7f", null ],
    [ "RollPlot_Init", "group__roll__plot.html#ga14ed3db489b62aa56a03b83140c119eb", null ],
    [ "RollPlot_SignalInit", "group__roll__plot.html#ga3ab39a0d22952095e0068a93cb2cd6ce", null ]
];