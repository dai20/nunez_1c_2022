var searchData=
[
  ['back_5fcolor',['back_color',['../structplot__t.html#a649b9f5d9de03ef2fc6e6431546e469a',1,'plot_t']]],
  ['baud_5frate',['baud_rate',['../structserial__config.html#ae28e9ec22e61f2fd869555513c0857c0',1,'serial_config']]],
  ['bit_5fn',['bit_n',['../structbit__n.html',1,'']]],
  ['bit_5fsend',['bit_send',['../structbit__send.html',1,'']]],
  ['bitrate',['bitrate',['../structspi_config__t.html#a9f84d708b15f0c2f9fdd97916673e25d',1,'spiConfig_t']]],
  ['bool',['bool',['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html',1,'(Global Namespace)']]],
  ['buzzer',['Buzzer',['../group___buzzer.html',1,'']]],
  ['buzzerdeinit',['BuzzerDeinit',['../group___buzzer.html#gaafca49247486fd85cffa03e65e1cc855',1,'BuzzerDeinit(void):&#160;buzzer.c'],['../group___buzzer.html#gaafca49247486fd85cffa03e65e1cc855',1,'BuzzerDeinit(void):&#160;buzzer.c']]],
  ['buzzerinit',['BuzzerInit',['../group___buzzer.html#gae2c0e517a4e17474f391a009e5c4590b',1,'BuzzerInit(void):&#160;buzzer.c'],['../group___buzzer.html#gae2c0e517a4e17474f391a009e5c4590b',1,'BuzzerInit(void):&#160;buzzer.c']]],
  ['buzzeroff',['BuzzerOff',['../group___buzzer.html#ga5044339a4b4f968150ea884a334ddbd8',1,'BuzzerOff(void):&#160;buzzer.c'],['../group___buzzer.html#ga5044339a4b4f968150ea884a334ddbd8',1,'BuzzerOff(void):&#160;buzzer.c']]],
  ['buzzeron',['BuzzerOn',['../group___buzzer.html#ga5c945426454a12192b5dd46a36f45310',1,'BuzzerOn(void):&#160;buzzer.c'],['../group___buzzer.html#ga5c945426454a12192b5dd46a36f45310',1,'BuzzerOn(void):&#160;buzzer.c']]],
  ['buzzersetfrec',['BuzzerSetFrec',['../group___buzzer.html#ga082170de2e447ecd09c20c8cdee5826c',1,'BuzzerSetFrec(uint16_t freq):&#160;buzzer.c'],['../group___buzzer.html#ga082170de2e447ecd09c20c8cdee5826c',1,'BuzzerSetFrec(uint16_t freq):&#160;buzzer.c']]]
];
