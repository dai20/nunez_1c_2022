var indexSectionsWithContent =
{
  0: "_abcdfghilmnprstuvwxy",
  1: "abdfmprst",
  2: "fgsw",
  3: "abdghilmnprstuw",
  4: "bcdfghimpstwxy",
  5: "cgilpst",
  6: "cgims",
  7: "p",
  8: "abdfghilmnprstuvw",
  9: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

