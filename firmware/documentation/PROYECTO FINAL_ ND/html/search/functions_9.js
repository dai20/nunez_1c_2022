var searchData=
[
  ['pwmdeinit',['PWMDeinit',['../group___p_w_m.html#gaf0bd834d21081a27e8c0d0df14e558e2',1,'PWMDeinit(void):&#160;pwm_sct.c'],['../group___p_w_m.html#gaf0bd834d21081a27e8c0d0df14e558e2',1,'PWMDeinit(void):&#160;pwm_sct.c']]],
  ['pwminit',['PWMInit',['../group___p_w_m.html#gaaa8f960523fc95ecfcc422e5f5843e01',1,'PWMInit(pwm_out_t *ctout, uint8_t n_outs, uint16_t freq):&#160;pwm_sct.c'],['../group___p_w_m.html#gaaa8f960523fc95ecfcc422e5f5843e01',1,'PWMInit(pwm_out_t *ctout, uint8_t n_outs, uint16_t freq):&#160;pwm_sct.c']]],
  ['pwmoff',['PWMOff',['../group___p_w_m.html#ga22c7eecc5b183661ce6a5649f455d45f',1,'PWMOff(void):&#160;pwm_sct.c'],['../group___p_w_m.html#ga22c7eecc5b183661ce6a5649f455d45f',1,'PWMOff(void):&#160;pwm_sct.c']]],
  ['pwmon',['PWMOn',['../group___p_w_m.html#ga90a0a0967bf7e8f0ed14da97988c6d71',1,'PWMOn(void):&#160;pwm_sct.c'],['../group___p_w_m.html#ga90a0a0967bf7e8f0ed14da97988c6d71',1,'PWMOn(void):&#160;pwm_sct.c']]],
  ['pwmsetdutycycle',['PWMSetDutyCycle',['../group___p_w_m.html#ga2df6527368e723f864d17549bfd04997',1,'PWMSetDutyCycle(pwm_out_t ctout, uint8_t duty_cycle):&#160;pwm_sct.c'],['../group___p_w_m.html#ga2df6527368e723f864d17549bfd04997',1,'PWMSetDutyCycle(pwm_out_t ctout, uint8_t duty_cycle):&#160;pwm_sct.c']]],
  ['pwmsetfreq',['PWMSetFreq',['../group___p_w_m.html#ga39385387144d35a7b664080b29e2a873',1,'PWMSetFreq(uint32_t freq):&#160;pwm_sct.c'],['../group___p_w_m.html#ga39385387144d35a7b664080b29e2a873',1,'PWMSetFreq(uint32_t freq):&#160;pwm_sct.c']]]
];
