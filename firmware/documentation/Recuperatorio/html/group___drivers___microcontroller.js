var group___drivers___microcontroller =
[
    [ "Analog IO", "group___analog___i_o.html", "group___analog___i_o" ],
    [ "GPIO", "group___g_i_o_p.html", "group___g_i_o_p" ],
    [ "I2C", "group___i2_c.html", "group___i2_c" ],
    [ "PWM", "group___p_w_m.html", "group___p_w_m" ],
    [ "RTC", "group___r_t_c.html", "group___r_t_c" ],
    [ "SPI", "group___s_p_i.html", "group___s_p_i" ],
    [ "Systemclock", "group___systemclock.html", "group___systemclock" ],
    [ "Timer", "group___timer.html", "group___timer" ],
    [ "UART", "group___u_a_r_t.html", "group___u_a_r_t" ]
];