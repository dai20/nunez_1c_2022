var searchData=
[
  ['panaloginput',['pAnalogInput',['../structanalog__input__config.html#a247d18bf22d03f3d67a47647bbf1d1ee',1,'analog_input_config']]],
  ['period',['period',['../structtimer__config.html#abd839b0572ca4c62c0e6137bf6fbe4a1',1,'timer_config']]],
  ['pfunc',['pFunc',['../structtimer__config.html#afaafe064d9f374c518774856f9fd1726',1,'timer_config']]],
  ['pixel',['pixel',['../structpixel.html',1,'']]],
  ['plot',['plot',['../structsignal__t.html#acd4921f0319654d419cff2eff70e054e',1,'signal_t']]],
  ['plot_5ft',['plot_t',['../structplot__t.html',1,'']]],
  ['port',['port',['../structspi_config__t.html#a1545c77f8baf52da6267c237d5c501ba',1,'spiConfig_t::port()'],['../structserial__config.html#a5430eba070493ffcc24c680a8cce6b55',1,'serial_config::port()']]],
  ['port_5fmiso1',['PORT_MISO1',['../spi_8c.html#a4e01e480acbe18712f1f04872274f1d0',1,'spi.c']]],
  ['port_5fmosi1',['PORT_MOSI1',['../spi_8c.html#a33c77f2ef81471566b5182ea5ae1af08',1,'spi.c']]],
  ['port_5fsck1',['PORT_SCK1',['../spi_8c.html#acf40fb602fef2a392a65dcc6e01b8937',1,'spi.c']]],
  ['pserial',['pSerial',['../structserial__config.html#a3f4ce60ef262396e928d64249813b659',1,'serial_config']]],
  ['ptr_5fgpio_5fgroup_5fint_5ffunc',['ptr_GPIO_group_int_func',['../group___g_i_o_p.html#gadb1b43449a7ec81462b1e8ae68041b50',1,'gpio.c']]],
  ['ptr_5fgpio_5fint_5ffunc',['ptr_GPIO_int_func',['../group___g_i_o_p.html#gac7d9672849de0a3c41c38280af236661',1,'gpio.c']]],
  ['pwm',['PWM',['../group___p_w_m.html',1,'']]],
  ['pwm_5fout_5ft',['pwm_out_t',['../group___p_w_m.html#gaf28a35f298e221200e47ba03ed074eee',1,'pwm_sct.h']]],
  ['pwmdeinit',['PWMDeinit',['../group___p_w_m.html#gaf0bd834d21081a27e8c0d0df14e558e2',1,'PWMDeinit(void):&#160;pwm_sct.c'],['../group___p_w_m.html#gaf0bd834d21081a27e8c0d0df14e558e2',1,'PWMDeinit(void):&#160;pwm_sct.c']]],
  ['pwminit',['PWMInit',['../group___p_w_m.html#gaaa8f960523fc95ecfcc422e5f5843e01',1,'PWMInit(pwm_out_t *ctout, uint8_t n_outs, uint16_t freq):&#160;pwm_sct.c'],['../group___p_w_m.html#gaaa8f960523fc95ecfcc422e5f5843e01',1,'PWMInit(pwm_out_t *ctout, uint8_t n_outs, uint16_t freq):&#160;pwm_sct.c']]],
  ['pwmoff',['PWMOff',['../group___p_w_m.html#ga22c7eecc5b183661ce6a5649f455d45f',1,'PWMOff(void):&#160;pwm_sct.c'],['../group___p_w_m.html#ga22c7eecc5b183661ce6a5649f455d45f',1,'PWMOff(void):&#160;pwm_sct.c']]],
  ['pwmon',['PWMOn',['../group___p_w_m.html#ga90a0a0967bf7e8f0ed14da97988c6d71',1,'PWMOn(void):&#160;pwm_sct.c'],['../group___p_w_m.html#ga90a0a0967bf7e8f0ed14da97988c6d71',1,'PWMOn(void):&#160;pwm_sct.c']]],
  ['pwmsetdutycycle',['PWMSetDutyCycle',['../group___p_w_m.html#ga2df6527368e723f864d17549bfd04997',1,'PWMSetDutyCycle(pwm_out_t ctout, uint8_t duty_cycle):&#160;pwm_sct.c'],['../group___p_w_m.html#ga2df6527368e723f864d17549bfd04997',1,'PWMSetDutyCycle(pwm_out_t ctout, uint8_t duty_cycle):&#160;pwm_sct.c']]],
  ['pwmsetfreq',['PWMSetFreq',['../group___p_w_m.html#ga39385387144d35a7b664080b29e2a873',1,'PWMSetFreq(uint32_t freq):&#160;pwm_sct.c'],['../group___p_w_m.html#ga39385387144d35a7b664080b29e2a873',1,'PWMSetFreq(uint32_t freq):&#160;pwm_sct.c']]]
];
