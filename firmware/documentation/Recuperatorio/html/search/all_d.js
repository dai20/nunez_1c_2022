var searchData=
[
  ['readxvalue',['ReadXValue',['../group___m_m_a7260_q.html#gac62564c41a96ceca5b38d6b73ef4c6f9',1,'MMA7260Q.h']]],
  ['readyvalue',['ReadYValue',['../group___m_m_a7260_q.html#ga25995f82b10255c8466cbb01308ae375',1,'MMA7260Q.h']]],
  ['readzvalue',['ReadZValue',['../group___m_m_a7260_q.html#gace645c520fdd92b45b72c711e26fe9e0',1,'MMA7260Q.h']]],
  ['record',['Record',['../struct_record.html',1,'']]],
  ['roll_20plot',['Roll Plot',['../group__roll__plot.html',1,'']]],
  ['rollplot_5fdraw',['RollPlot_Draw',['../group__roll__plot.html#gabea6b076c1f8b324f3d85bc8c1d79f7f',1,'RollPlot_Draw(signal_t *signal, uint16_t data):&#160;roll_plot.c'],['../group__roll__plot.html#gabea6b076c1f8b324f3d85bc8c1d79f7f',1,'RollPlot_Draw(signal_t *signal, uint16_t data):&#160;roll_plot.c']]],
  ['rollplot_5finit',['RollPlot_Init',['../group__roll__plot.html#ga14ed3db489b62aa56a03b83140c119eb',1,'RollPlot_Init(plot_t *plot):&#160;roll_plot.c'],['../group__roll__plot.html#ga14ed3db489b62aa56a03b83140c119eb',1,'RollPlot_Init(plot_t *plot):&#160;roll_plot.c']]],
  ['rollplot_5fsignalinit',['RollPlot_SignalInit',['../group__roll__plot.html#ga3ab39a0d22952095e0068a93cb2cd6ce',1,'RollPlot_SignalInit(plot_t *plot, signal_t *signal):&#160;roll_plot.c'],['../group__roll__plot.html#ga3ab39a0d22952095e0068a93cb2cd6ce',1,'RollPlot_SignalInit(plot_t *plot, signal_t *signal):&#160;roll_plot.c']]],
  ['rtc',['RTC',['../group___r_t_c.html',1,'']]],
  ['rtc_5ft',['rtc_t',['../structrtc__t.html',1,'']]]
];
