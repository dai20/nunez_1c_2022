var searchData=
[
  ['ch0',['CH0',['../group___analog___i_o.html#gaf6f592bd18a57b35061f111d32a7f637',1,'analog_io.h']]],
  ['clk_5fmode',['clk_mode',['../structspi_config__t.html#a668d1a324fd6229b02031f527ba616f9',1,'spiConfig_t']]],
  ['clkmode_5ft',['clkMode_t',['../group___s_p_i.html#ga2e3236199f0007eb5a75e17be158613b',1,'spi.h']]],
  ['color',['color',['../structsignal__t.html#a1c250d9df690cb15a2b504c39f3aebb6',1,'signal_t']]],
  ['ctout0',['CTOUT0',['../group___p_w_m.html#ggaf28a35f298e221200e47ba03ed074eeeaaf59de6ad393c11f5248c9c3c7f83675',1,'pwm_sct.h']]],
  ['ctout1',['CTOUT1',['../group___p_w_m.html#ggaf28a35f298e221200e47ba03ed074eeea8083c36b94c860ac559b1181ab9d4dd4',1,'pwm_sct.h']]],
  ['ctout2',['CTOUT2',['../group___p_w_m.html#ggaf28a35f298e221200e47ba03ed074eeea3dbae904eca1c2c1e7266d59f419edcf',1,'pwm_sct.h']]],
  ['ctout3',['CTOUT3',['../group___p_w_m.html#ggaf28a35f298e221200e47ba03ed074eeea601548a16d4016c036d1fcb76efb6ea1',1,'pwm_sct.h']]]
];
