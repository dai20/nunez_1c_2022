var searchData=
[
  ['tcrt5000deinit',['Tcrt5000Deinit',['../group___t_c_r_t5000.html#gaea70ef203d4e224e6fe79a5f5ad12f1a',1,'Tcrt5000Deinit(void):&#160;tcrt5000.c'],['../group___t_c_r_t5000.html#gaea70ef203d4e224e6fe79a5f5ad12f1a',1,'Tcrt5000Deinit(void):&#160;tcrt5000.c']]],
  ['tcrt5000init',['Tcrt5000Init',['../group___t_c_r_t5000.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;tcrt5000.c'],['../group___t_c_r_t5000.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;tcrt5000.c']]],
  ['tcrt5000readstate',['Tcrt5000ReadState',['../group___t_c_r_t5000.html#ga2c369ef1636ab199272b5597c0f0eff1',1,'Tcrt5000ReadState(void):&#160;tcrt5000.c'],['../group___t_c_r_t5000.html#ga2c369ef1636ab199272b5597c0f0eff1',1,'Tcrt5000ReadState(void):&#160;tcrt5000.c']]],
  ['timerinit',['TimerInit',['../group___timer.html#ga148b01475111265d1798f5c204a93df0',1,'TimerInit(timer_config *timer_ini):&#160;timer.c'],['../group___timer.html#ga148b01475111265d1798f5c204a93df0',1,'TimerInit(timer_config *timer_ini):&#160;timer.c']]],
  ['timerreset',['TimerReset',['../group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'TimerReset(uint8_t timer):&#160;timer.c'],['../group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'TimerReset(uint8_t timer):&#160;timer.c']]],
  ['timerstart',['TimerStart',['../group___timer.html#ga31487bffd934ce838a72f095f9231b24',1,'TimerStart(uint8_t timer):&#160;timer.c'],['../group___timer.html#ga31487bffd934ce838a72f095f9231b24',1,'TimerStart(uint8_t timer):&#160;timer.c']]],
  ['timerstop',['TimerStop',['../group___timer.html#gab652b899be3054eae4649a9063ec904b',1,'TimerStop(uint8_t timer):&#160;timer.c'],['../group___timer.html#gab652b899be3054eae4649a9063ec904b',1,'TimerStop(uint8_t timer):&#160;timer.c']]]
];
