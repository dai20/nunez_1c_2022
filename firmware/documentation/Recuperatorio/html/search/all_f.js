var searchData=
[
  ['template',['Template',['../index.html',1,'']]],
  ['tcrt5000',['TCRT5000',['../group___t_c_r_t5000.html',1,'']]],
  ['tcrt5000deinit',['Tcrt5000Deinit',['../group___t_c_r_t5000.html#gaea70ef203d4e224e6fe79a5f5ad12f1a',1,'Tcrt5000Deinit(void):&#160;tcrt5000.c'],['../group___t_c_r_t5000.html#gaea70ef203d4e224e6fe79a5f5ad12f1a',1,'Tcrt5000Deinit(void):&#160;tcrt5000.c']]],
  ['tcrt5000init',['Tcrt5000Init',['../group___t_c_r_t5000.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;tcrt5000.c'],['../group___t_c_r_t5000.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;tcrt5000.c']]],
  ['tcrt5000readstate',['Tcrt5000ReadState',['../group___t_c_r_t5000.html#ga2c369ef1636ab199272b5597c0f0eff1',1,'Tcrt5000ReadState(void):&#160;tcrt5000.c'],['../group___t_c_r_t5000.html#ga2c369ef1636ab199272b5597c0f0eff1',1,'Tcrt5000ReadState(void):&#160;tcrt5000.c']]],
  ['timer',['timer',['../structtimer__config.html#ab42a22a518af439f16e4d09e51aa2553',1,'timer_config::timer()'],['../group___timer.html',1,'(Global Namespace)']]],
  ['timer_5fconfig',['timer_config',['../structtimer__config.html',1,'']]],
  ['timerinit',['TimerInit',['../group___timer.html#ga148b01475111265d1798f5c204a93df0',1,'TimerInit(timer_config *timer_ini):&#160;timer.c'],['../group___timer.html#ga148b01475111265d1798f5c204a93df0',1,'TimerInit(timer_config *timer_ini):&#160;timer.c']]],
  ['timerreset',['TimerReset',['../group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'TimerReset(uint8_t timer):&#160;timer.c'],['../group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'TimerReset(uint8_t timer):&#160;timer.c']]],
  ['timerstart',['TimerStart',['../group___timer.html#ga31487bffd934ce838a72f095f9231b24',1,'TimerStart(uint8_t timer):&#160;timer.c'],['../group___timer.html#ga31487bffd934ce838a72f095f9231b24',1,'TimerStart(uint8_t timer):&#160;timer.c']]],
  ['timerstop',['TimerStop',['../group___timer.html#gab652b899be3054eae4649a9063ec904b',1,'TimerStop(uint8_t timer):&#160;timer.c'],['../group___timer.html#gab652b899be3054eae4649a9063ec904b',1,'TimerStop(uint8_t timer):&#160;timer.c']]],
  ['transfer_5fmode',['transfer_mode',['../structspi_config__t.html#aef147d342aef67c4adf42dfa2c3a28c5',1,'spiConfig_t']]],
  ['transfermode_5ft',['transferMode_t',['../group___s_p_i.html#ga8eca2297218426636952c631c9a8c881',1,'spi.h']]],
  ['true',['true',['../group___bool.html#ga41f9c5fb8b08eb5dc3edce4dcb37fee7',1,'true():&#160;bool.h'],['../group___bool.html#ga41f9c5fb8b08eb5dc3edce4dcb37fee7',1,'true():&#160;bool.h']]]
];
