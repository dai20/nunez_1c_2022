/* Copyright 2022,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA NUÑEZ
*/
/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * Este driver:
 * 	- Inicializa el dispositivo SevomotorSG90.
 * 	- Mueve el servomotor según los datos que recibe.
 * 	- Deinicializa el dispositivo SevomotorSG90.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |    Servomotor SG90	|   EDU-CIAA	|
 * |:------------------:|:--------------|
 * |	PWM	  	  		| 	T_FIL2		|
 * | 	5V	 			| 	5V			|
 * | 	GND	 			| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/06/2022 | Document creation		                         |
 * | 			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA NUÑEZ
 *
 */


#ifndef SERVO_SG90_H
#define SERVO_SG90_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[macros and definitions]=================================*/

/*==================[typedef]================================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


/**
* @fn void ServoSInit(void)
* @brief Initialization function of Servomotor SG90.
* @param[in] No parameter.
* @return null.
*/
void ServoSG90Init(void);

/**
* @fn void ServoSG90Move(int16_t grados);
* @brief It moves the servomotor according to the degrees it receives.//Mueve el servomotor de acuerdo a los grados que recibe.
* @param[in] grados.
* @return null
*/
void ServoSG90Move(int16_t grados);

/**
* @fn void ServoSG90Deinit(void)
* @brief Deinitialization function of  Servomotor SG90.
* @return null
*/
void ServoSG90Deinit(void);

/*==================[end of file]============================================*/

#endif /* SERVO_SG90_H */
