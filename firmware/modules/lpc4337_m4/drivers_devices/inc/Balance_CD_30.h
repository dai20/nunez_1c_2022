/* Copyright 2022,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA  NUÑEZ
*/
/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * 	Este driver:
 * 	- Inicializa el dispositivo Balanza modelo CD30.
 * 	- Digitaliza la señal analógica proveniente de la balanza modelo CD30.
 * 	La señal ingresa por el canal CH1 del conver AD.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 *
 * |  Balanza_CD30		|   EDU-CIAA	|
 * |(Sensor analógico)	|     			|
 * |:------------------:|:--------------|
 * | 	VCC		 		| 	 5V			|
 * | 	OUT		 		| 	CH1			|
 * | 	GND		 		| 	GNDA		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 19/05/2022 | Document creation		                         |
 * | 			|								                 |
 * | 			| 	                     						 |
 *
 * @author DAIANA  NUÑEZ
 *
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_BALANCE_CD_30_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_BALANCE_CD_30_H_

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[macros and definitions]=================================*/

/*==================[typedef]================================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

/**
* @fn void BalanceCD30Init(void)
* @brief Initialization function of  CD_30 Balance.
* @param[in] No parameter.
* @return null.
*/
void BalanceCD30Init(void);

/**
* @fn void BalanceCD30Read(uint8_t channel,  uint16_t *value)
* @brief Converts data from analog to digital format.//Convierte los datos de formato analógico a digital.
* @return null
*/
uint16_t BalanceCD30Read(void);


/*==================[end of file]============================================*/

#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_BALANCE_CD_30_H_ */
