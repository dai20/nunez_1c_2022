/* Copyright 2022,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA NUÑEZ
*/
/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |  Balanza_CD30		|   EDU-CIAA	|
 * |(Sensor analógico)	|     			|
 * |:------------------:|:--------------|
 * | 	VCC		 		| 	 5V			|
 * | 	OUT		 		| 	CH1			|
 * | 	GND		 		| 	GNDA		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/05/2020 | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA  NUÑEZ
 *
 */


/*==================[inclusions]=============================================*/
#include "Balance_CD_30.h"       /* <= own header */

#include "analog_io.h"
#include "systemclock.h"
//#include "timer.h"
//#include "uart.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/

/*==================[typedef]================================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
//INICIALIZO la estructura del ADC:
analog_input_config analog_myStructureInput= {CH1,//lo pongo como canal genérico
					 	 	 	 	 	 	 AINPUTS_SINGLE_READ,
											 NULL};// AnalogStartConvertion()

uint16_t value;


/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


void BalanceCD30Init(void)
{
	AnalogInputInit(&analog_myStructureInput);
}

uint16_t BalanceCD30Read (void)
{
	uint16_t gramos;
	AnalogInputReadPolling(CH1, &value);//Debo pasar la variable por referecina &value donde quiero que guarde el dato
	//del peso.

	//Implementar la función para pasar el valor de tensión a peso.
	//y=mx+b;
	gramos = (uint16_t)(-2.1608*(float)value + 1764.8);
	return gramos;
}






/*==================[end of file]============================================*/

