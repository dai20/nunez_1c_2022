/* Copyright 2022,
* Juan Manuel Reta
* jmreta@ingenieria.uner.edu.ar
* Cátedra Electrónica Programable
* Facultad de Ingenieria
* Universidad Nacional de Entre Rios
* Argentina.
*
* All rights reserved.
*
* This file is part of CIAA Firmware.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*

*
* Initials     Name
* ---------------------------
* DN		DAIANA NUÑEZ
*/
/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Servomotor SG90	|   EDU-CIAA	|
 * |:------------------:|:--------------|
 * | 	PWM	 			| 	T_FIL2		|
 * | 	5V	 			| 	5V			|
 * | 	GND	 			| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/06/2020 | Document creation		                         |
 * |			| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author DAIANA NUÑEZ
 *
 */


/*==================[inclusions]=============================================*/
#include "Servo_SG90.h"       /* <= own header */

#include "gpio.h"
#include "pwm_sct.h"

#include <stdint.h>
/*==================[macros and definitions]=================================*/

/*Información de datasheet del servomotor SG90*/

#define PeriodoTotal 20.0 /*Periodo Total en mseg*/
#define Frecuencia 50 /*Frecuencia 50Hz*/

/*==================[typedef]================================================*/
/*==================[internal data declaration]==============================*/
uint16_t grados;
float TiempoAlto;
uint8_t CicloTrabajo;
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

uint8_t NroOutput=1;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void ServoSG90Init(void)
{
	pwm_out_t pon[]= {CTOUT0}; //Debe programarse así, para evitar conflictos con capa inferior.
	PWMInit(pon, NroOutput, Frecuencia);
	PWMOn();
}

void ServoSG90Move(int16_t grados)
{
	TiempoAlto =grados;
	grados=grados+90;
	TiempoAlto=((float)grados/180.0)+1.5; //TiempoAlto=1 => -45°. TiempoAlto=1,5 => 0°. TiempoAlto=2 => 45°.
	CicloTrabajo=(uint16_t)((TiempoAlto/PeriodoTotal)*100.0);
	PWMSetDutyCycle(CTOUT0, CicloTrabajo);

}

void ServoSG90Deinit(void)
{
	bool PWMDeinit(void);
}







/*==================[end of file]============================================*/

